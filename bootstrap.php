<?php

// return function () {
//     echo 'Hello, world!';
// };

use Flarum\Event\ConfigureClientView;

$events->listen(ConfigureClientView::class, function (ConfigureClientView $event) {
    if ($event->isForum()) {
        $event->addAssets(__DIR__.'/js/forum/dist/extension.js');
        $event->addBootstrapper('endylan/flarum-ext-flarloader/main');
    }
});