var flarum = require('flarum-gulp');

flarum({
  modules: {
    'endylan/flarum-ext-flarloader': [
      'src/**/*.js'
    ]
  }
});